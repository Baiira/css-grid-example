import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grid-container',
  templateUrl: './grid-container.component.html',
  styleUrls: ['./grid-container.component.css']
})
export class GridContainerComponent implements OnInit {
  gridViewer = 'grid-viewer';
  gridForm = 'grid-form';
  gridTree = 'grid-tree';
  constructor() { }

  ngOnInit() {
  }

}
