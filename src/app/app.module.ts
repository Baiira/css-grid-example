import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GridContainerComponent } from './grid-container/grid-container.component';
import { GridFormComponent } from './grid-form/grid-form.component';
import { GridViewerComponent } from './grid-viewer/grid-viewer.component';
import { GridTreeComponent } from './grid-tree/grid-tree.component';

@NgModule({
  declarations: [
    AppComponent,
    GridContainerComponent,
    GridFormComponent,
    GridViewerComponent,
    GridTreeComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
