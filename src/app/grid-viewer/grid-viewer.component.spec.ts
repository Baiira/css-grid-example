import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GridViewerComponent } from './grid-viewer.component';

describe('GridViewerComponent', () => {
  let component: GridViewerComponent;
  let fixture: ComponentFixture<GridViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GridViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
